## Animal is-a object look at the extra credit
class Animal
end

## is-a
class Dog < Animal

  def initialize(name)
    ## has-a
    @name = name
  end
end

## has-a
class Cat < Animal

  def initialize(name)
    ## has-a name
    @name = name
  end
end

## is-a person
class Person

  def initialize(name)
    ## has-a name
    @name = name

    ## Person has-a pet of some kind
    @pet = nil
  end

  attr_accessor :pet
end

## is-a
class Employee < Person

  def initialize(name, salary)
    ## has-a hmm what is this strange magic?
    super(name)
    ## has-a salary
    @salary = salary
  end

end

## is-a
class Fish
end

## is-a
class Salmon < Fish
end

## is-a
class Halibut < Fish
end


## rover is-a Dog
rover = Dog.new("Rover")

## is-a Cat
satan = Cat.new("Satan")

## is-a Person
mary = Person.new("Mary")

## has-a pet that is-a cat
mary.pet = satan

## is-a Employee
frank = Employee.new("Frank", 120000)

## frank has-a Dog
frank.pet = rover

## is-a Fish
flipper = Fish.new()

## is-a Salmon
crouse = Salmon.new()

## is-a Halibut
harry = Halibut.new()