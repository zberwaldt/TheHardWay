# create a mapping of state to abbreviation
states = {
    'Oregon' => 'OR',
    'Florida' => 'FL',
    'California' => 'CA',
    'New York' => 'NY',
    'Michigan' => 'MI'
}

# create a basic set of states and some cities in them
cities = {
    'CA' => 'San Francisco',
    'MI' => 'Detriot',
    'FL' => 'Jacksonville',
}

# add some more cities
cities['NY'] = 'New York'
cities['OR'] = 'Portland'

# puts out some cities
puts '-' * 10
puts "NY State has: #{cities['NY']}"
puts "OR State has: #{cities['OR']}"

# puts some states
puts '-' * 10
puts "Michigan's abbreviation is: #{states['Michigan']}"
puts "Florida's abbreviattion is: #{states['Florida']}"

# puts every state abbreviation
puts '-' * 10
states.each do |state, abbrev|
    puts "#{state} is abbreviated #{abbrev}"
end

# puts every city in state
puts '-' * 10
cities.each do |abbrev, city|
    puts "#{abbrev} has the city #{city}"
end

# now do both at the same time
puts '-' * 10
states.each do |state, abbrev|
    city = cities[abbrev]
    puts "#{state} is abbreviated #{abbrev} and has city #{city}"
end

puts '-' * 10

# by default ruby says "nil" when something isn't there
state = states['Texas']

if !state
    puts "Sorry, no Texas."
end

# default values using ||= with the nil result
city = cities['TX']
city ||= 'Does Not Exist'
puts "The city for state 'TX' is: #{city}"

# adding my own implementation of this.
puts '-' * 20 # no implicit conversion of Hash into Integer (TypeError): Was because I forgot to add 20 to the end of this puts statement


# don't forget, in Ruby, you don't need to end with a semi colon

statesZb = Hash.new # set statesZb to a new Hash or dictionary with a default value of "MA"
statesZb.default = "No such state."
puts statesZb # should output "MA" by default ... did not just output an empty dictionary. In hind sight this makes sense
puts statesZb['Montana'] # this outputs the default value of the Hash, because Montana is not present

statesZb = {
    'Massachussetts' => 'MA',
    'New York' => 'NY',
    'New Hampshire' => 'NH',
    'Maine' => 'ME'
}

citiesZb = {
    'MA' => 'Boston',
    'NY' => 'New York',
    'NH' => 'Concord',
    'Maine' => 'Augusta'
}

statesZb.each do |state, abbrev| # accidentally switched the arguments of the loop...which is now fixed
    city = citiesZb[abbrev]
    puts city
end

