def looper(int, add)
  i = 0
  numbers = []

  while i < int
    puts "At the top i is #{i}"
    numbers.push(i)

    i += add
    puts "Numbers now: ", numbers
    puts "At the bottom i is #{i}"
  end

  puts "The numbers: "
end

looper(3, 6)
looper(7, 2)
looper(2, 3)
