# Delcare variables tabby_cat, persian_cat, and backslash_cat
tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = " I'm \\ a \\ cat."

# Declare fat_cat, which uses triple double quotes to illustrate it's capabilities
fat_cat = """
I'll do a list:
\t* Cat food
\t* Fisheries
\t* Catnip \n\t* Grass
"""

# The follwoing four lines puts the four variables declared above to console
puts tabby_cat
puts persian_cat
puts backslash_cat
puts fat_cat
