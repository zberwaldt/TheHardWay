module Control

  def Control.control
    puts "You enter the room designated \"CONTROL\""
        puts "You see a giant red button on a console in the center of the room."
        puts "You can PUSH the button or go BACK."
    
    still_here = false
    while true
      puts "What are you going to do?"
      print "> "
      choice = $stdin.gets.chomp

      if choice.include? "push"
        puts "You push the button."
                puts "\" YOU HAVE ACTIVATED SAFETY PROTOCOL...REACTOR RETURNING TO OPERATIONAL STATUS!\" says the A.I."
        sleep 2
        puts "You curse loudly, realizing that this has to be the most anti-climatic adventure you've ever been on."
        sleep 2
        puts "You go back down the corridor and find that the room to the crew quarters is no longer locked."
        sleep 2
        puts "You crawl back into your bed and go back to sleep."
        exit(0)
      elsif choice.include?  "back"
        puts "You go back to the corridor"
        CorridorOne.corridor_one
      else
        puts "You're running out of time!"
        puts "Make a decision, already!"
        still_here = true
              end
    end
  end
end
