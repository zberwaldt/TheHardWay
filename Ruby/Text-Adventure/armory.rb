module Armory

  def Armory.armory
      puts "You step into, what appears to be, the armory."
            puts "The room is full of weapons and equipment in lockers."
            puts " There are no other doors to other rooms that you can see."
            puts "You can try to OPEN a locker or go BACK"
      
      still_here = false
      tried = false

      while true
        puts "What are you going to do?"
        print "> "

        choice = $stdin.gets.chomp
        if choice.include? "open" && tried == false
          puts "You try to open a locker near by..."
                    puts "Alarm lights start flashing, no alarm sounds because the alarms are going off because of the reactor breach, remember?"
                    puts "the station A.I. shouts a new warning:"
          sleep 1
          puts "\" YOU DO NOT HAVE CLEARANCE TO ACCESS THE ARMORY \""
          tried = true
          still_here = true
          sleep 1
        elsif choice.include? "open" && tried == true
          puts "You try to open another locker near by..."
                    Dead.dead("Laser turrets pop out of the cieling and melt you into splooge.")
        elsif choice.include? "back"
          puts "You go back out the way you came in..."
                    CorridorOne.corridor_one
        else
          puts "You're running out of time!"
          puts "Make a decision, already!"
          still_here = true
                  end

      end

  end

end
