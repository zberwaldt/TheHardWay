# Here is some new strange stuff, remember type it exactly.

# Declare variables for days and months
days = "Mon Tue Wed Thur Fri Sat Sun"
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

# Print days and months "\n" which I believe indicates a new line
puts "Here are the days: #{days}"
puts "Here are the months: #{months}"

# Prints multiple lines to the console without using quotes
puts %q{
  There's something going on here.
  With the three double quotes.
  We'll be able to type as much as we want.
  Even 4 lines if we want, or 5, or 6.
}
