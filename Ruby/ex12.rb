# Print a request for a number.
print "Give me a number: "
# Assign user input to the variable number.
number = gets.chomp.to_i

# Multiply number by 100 and assign it to the bigger variable.
bigger = number * 100
# Put a sting with the bigger variable formatted into the string.
puts "A bigger number is #{bigger}."

# Print a request for another number.
print "Give me another number: "
# Assign this new number to the variable another
another = gets.chomp
# Convert another to an integer and assign it to the number variable.
number = another.to_i

# I added this comment and line after the fact to convert number to a float
number = number.to_f
# Declare variable smaller and assign number divided by 100 to it.
smaller = number / 100.to_f
# Put a string with the smaller variable formatted into the string.
puts "A smaller number is #{smaller}."

# Made an error writing this where I accidentally put a period outside the double quotes
# This caused my program to throw and error when I tried to run it.
