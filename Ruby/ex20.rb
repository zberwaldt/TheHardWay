# Take the first unit from the ARGV and assign it to input_file
input_file = ARGV.first

# Define function
def print_all(f)
  puts f.read
end

# Define function
def rewind(f)
 f.seek(0)
end

# Define function
def print_a_line(line_count, f)
 puts "#{line_count}, #{f.gets.chomp}"
end

# assign the input_file and open it into current_file.
current_file = open(input_file)

puts "First let's print the whole file:\n"

# Call the print_all function on current_file.
print_all(current_file)

puts "Now lets rewind, kind of like a tape."

# Call the rewind function on current_file.
rewind(current_file)

puts "Lets print three lines:"

current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)
