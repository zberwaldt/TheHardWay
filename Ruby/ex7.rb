# puts strings to console.
puts "Mary had a little lamb."
puts "Its fleece was white as #{'snow'}."
puts "And everywhere that Mary went."
# this prints out the period 10 times
puts "." * 10 # what's that do?

# Declare variables that contain the letters to "CheeseBurger"
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that print vs puts on this line what's it do?
#same as puts but does not create a new line after
print end1 + end2 + end3 + end4 + end5 + end6
#puts is short for "put string" and adds a new line after
puts end7 + end8 + end9 + end10 + end11 + end12
