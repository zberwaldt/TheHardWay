# Create a function.
def three_maths (a, b)
  sum = a + b
  puts sum
  diff = a - b
  puts diff
  prod = a * b
  puts prod
end
# Call the above function ten different ways.

num1 = 10
num2 = 500

three_maths(num1, num2)
three_maths(5, 6)
three_maths(10 + 18, 8 + 4)
three_maths(num1 + num2, num2 + num2)
three_maths(num1 + 6, num2 + 8)
three_maths(num1 * 2, num2)
three_maths(num1, num2 % 3)
three_maths(num1 ** 2, num1 + 1)
three_maths(num2, num2)
three_maths(num1, 8)
