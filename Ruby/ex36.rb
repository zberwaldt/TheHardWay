require_relative 'Text-Adventure/start'
require_relative 'Text-Adventure/corridor-one'
require_relative 'Text-Adventure/inventory'
require_relative 'Text-Adventure/armory'
require_relative 'Text-Adventure/dead'
require_relative 'Text-Adventure/control'

Start.start
