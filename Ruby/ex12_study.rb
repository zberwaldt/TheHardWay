# Print a request how much money you have with you.
print "How much money do you have right now? "
cash_i_have = gets.chomp.to_f

# put a string that confirms the amount you have
puts "So, you have #{cash_i_have} dollars with you."

# Declare cash_back variable and assign ten percent of the cash you have to it.
cash_back = cash_i_have * 0.1

# put a string that states the amount of cash you are being given back
puts "Here, you can have #{cash_back} back, I won't be needing it."
