# * Map
#     - next_scene
#     - opening_scene
# * Engine
#     - play
# * Scene
#     - enter
#     * Death
#     * Central Corridor
#     * Laser Weapon Armory
#     * The Bridge
#     * Escape Pod

class Scene
    def enter()
        puts "This scene is not yet configured. Subclass it and implement enter()."
        exit(1)
    end
end

class Engine
    def initialize(scene_map)
        @scene_map = scene_map
    end

    def play()
        current_scene = @scene_map.opening_scene()
        last_scene = @scene_map.next_scene('finished')

        while current_scene != last_scene
            next_scene_name = current_scene.enter()
            current_scene = @scene_map.next_scene(next_scene_name)
        end

        current_scene.enter()
    end
end

class Death < Scene
    def enter()
        puts "You have died!"
        exit(1)
    end
end

class CentralCorridor < Scene
    def enter()
        puts "You wake up in a central corridor of a ship. What do you do?"
        next_action = gets.chomp
        if next_action == "die"
            return "death"
        else 
            return 'death'
        end
    end

end

class LaserWeaponArmory < Scene
    def enter()
        return nil 
    end
end

class TheBridge < Scene
    def enter()
        return nil
    end
end

class EscapePod < Scene
    def enter()
        return nil
    end
end

class Map
    @@scenes = {
        'central_corridor' => CentralCorridor.new(),
        'laser_weapon_armory' => LaserWeaponArmory.new(),
        'escape_pod' => EscapePod.new(),
        'death' => Death.new()
    }

    def initialize(start_scene)
        @start_scene = start_scene
    end

    def next_scene(scene_name)
        val = @@scenes[scene_name]
        return val
    end

    def opening_scene()
        return next_scene(@start_scene)
    end
end

a_map = Map.new('central_corridor')
a_game = Engine.new(a_map)
a_game.play()
