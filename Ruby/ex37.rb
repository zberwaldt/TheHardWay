BEGIN { puts "Welcome to ex37.rb"}
  module Ex37
    def Ex37.logger(words)
      puts words
    end

    Ex37.logger("Hello!")
    
    def Ex37.hello(arr)
      arr.each do |x| 
        puts x 
      end
    end
	
    Ex37.hello(["hi", "bye", "array"])
  
  end
END { puts "Thanks for using this script!"}
