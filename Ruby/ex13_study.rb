# Exploring ARGV

# Declare fruit, vegetable, and meat and give it the ARGV
fruit, vegetable, meat = ARGV
# Declare input which takes an alternative form of gets.chomp to get user input
print "Are you enjoying ruby so far? "
input = $stdin.gets.chomp

# put formatted strings to console each with a single variable from above.
puts "The fruit you picked is: #{fruit}"
puts "The vegetable you picked is: #{vegetable}"
puts "The meat you picked is: #{meat}"
puts "You're final input was: #{input}"
