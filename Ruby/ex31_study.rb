puts "You wake up in a dark room. It's a med bay. As you get up the lights flicker on."
puts "You step out of the room into a corridor junction. You look up and see signs numbering the corridors. Do you go through corridor #1 or corridor #2?"

print "> "
corridor = $stdin.gets.chomp

if corridor == "1"
  puts "You walk down the corridor and come to an airlock. What do you do?"
  puts "1. Open the airlock."
  puts "2. Scream at the door"

  print "> "
  action = $stdin.gets.chomp

  if action == "1"
    puts "You get sucked out into space and die."
  elsif action == "2"
    puts "Alarms go off and an AI voice goes: 'Hostile detected'"
    puts "Laser come out of the ceiling and vaporize you."
  else
    puts "You wait and stare at the airlock for eternity"
  end

elsif corridor == "2"
  puts "You come to a mess hall full of people."
  puts "1. You shout \"HEY!\""
  puts "2. You get in line for food"
  puts "3. ..."

  print "> "
  action = $stdin.gets.chomp

  if action == "1" || action = "2"
    puts "nothing happens, you are dead and a ghost."
  else
    puts "You observe the people in the mess hall for eternity."
  end

else
  puts "You just stand at the junction for, like...ever."
end
