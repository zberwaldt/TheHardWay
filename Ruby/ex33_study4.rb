i = 0
numbers = []

(0..6).each do |i|
  puts "adding #{i} to the numbers array"
  puts "At the top i is #{i}"
  numbers.push(i)

  i += 1
  puts "Numbers now: ", numbers
  puts "At the bottom i is #{i}"
end

puts "The numbers: "
