# Declare user_name and give it the first input from ARGV.
user_name = ARGV.first
# Declare prompt and give it '> '.
prompt = '> '

# Put a greeting to the user to console.
puts "Hi #{user_name}."
# Put a statment about questions to come later in the program.
puts "I'd like to ask you a few questions."
# Put a inquiry about the level the user likes the program.
puts "Do you like me #{user_name}?"
# Put prompt to console.
puts prompt
# Take user input and assign it to likes variable.
likes = $stdin.gets.chomp

# Ask the user where he or she lives
puts "Where do you live #{user_name}?"
# Put prompt to console.
puts prompt
# Take user input and assign it ot lives variable.
lives = $stdin.gets.chomp

# Ask the user the type of computer it has.
# A comma for puts is like using it twice
puts "What kind of computer do you have #{user_name}", prompt
# Take user input and assign it to computer variable
computer = $stdin.gets.chomp

puts """
Alright, so you said #{likes} about liking me.
You live in #{lives}. Not sure where that is.
And you have a #{computer} computer. Nice.
"""
