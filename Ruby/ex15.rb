# Declare filename variable
print "what file do you wish to load? "
filename = $stdin.gets.chomp

# Delcare variable txt
txt = open(filename)

# Put a string that confirms the name of your file.
puts "Here is your file #{filename}"
# Print the contents of txt to console with the .read method.
print txt.read
# Close txt
txt.close()

# Declare txt_again which takes the file you specify.
# txt_again = open(file_again)

# Print the text_again contents with the .read method.
# print txt_again.read

# txt.close()
# text_again.close()
# Made an error where I declared txt_again but when I wrote a print command I wrote text_again instead.
