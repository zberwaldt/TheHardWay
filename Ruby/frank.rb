require 'sinatra'

set :public_folder, File.dirname(__FILE__) + '/static'

get '/' do
    erb :index
end

get '/about' do
    erb :about
end

get '/home' do
    'Hello, Home!'
end