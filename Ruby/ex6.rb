# Declare variables
types_of_people = 10
x = "There are #{types_of_people} types of people."
binary = "binary"
do_not = "don't"
y = "Those who know #{binary} and those who #{do_not}."

# Print variables x and y
puts x
puts y

# Print variables x and y in a string
puts "I said: #{x}."
puts "I also said: #{y}."

hilarious = false
joke_evaluation = "Isn't that joke so funny?! #{hilarious}"

puts joke_evaluation

# Declare variables for concatenation
w = "This is the left side of..."
e = "a string with a right side."

# Print concatenation of variables of w and e
puts w + e

# concatenation is a method in programing of joining strings together.
