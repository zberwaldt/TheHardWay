# This one is like one of your scripts with ARGV.
# Declare a function called print_two that takes a single parameter.
def print_two(*args)
  # Declare a local scope varibles called arg1 and arg2 that takes the arg argument as it's data.
  arg1,arg2 = args
  # Sting format the two above variables and put them to console.
  puts "arg1: #{arg1}, arg2: #{arg2}"
end

# Ok, that *args is pointless, we can just do this:
# Declare another function, it takes two arguments
def print_two_again(arg1,arg2)
  # Sting format them and put them to console.
  puts "arg1: #{arg1}, arg2: #{arg2}"
end

# This takes just one argument.
# Declare function.
def print_one(arg1)
  # When called this function puts it's argument in a formatted string.
  puts "arg1: #{arg1}"
end

# This one takes no arguements.
# Declare function.
def print_none()
  puts "I got nothin'."
end

# Call function.
print_two("Zed", "Shaw")
# Call function.
print_two_again("Zed", "Shaw")
# Call function.
print_one("First!")
# Call function.
print_none()
