# Declare filename which takes the first value from the ARGV array.
filename = ARGV.first

# put string to console that explains to the user what the program is about to do.
puts "We're going to erase #{filename}."
# Explain to user how to void this action, if they don't want to go through with it.
puts "If you don't want that, hit CTRL-C (^C)."
# Explain to the user how to go through with this action, if they want to.
puts "If you do want that, hit RETURN."

$stdin.gets

# Inform the user that the file is being opened.
puts "Opening the file..."
# Open the file.
target = open(filename, 'w')

# Inform the user that the file is getting truncated.
puts "Truncating the file. Goodbye!"
# Truncate the file.
target.truncate(0)

# Inform the user that they will be asked for three more lines.
puts "Now I am going to ask you for three more lines."

# Put sting to console
print "line 1: "
# Assign input to line1
line1 = $stdin.gets.chomp
# Put sting to console
print "line 2: "
# Assign input to line2
line2 = $stdin.gets.chomp
# Put sting to console
print "line 3: "
# Assign input to line3.
line3 = $stdin.gets.chomp

# Inform the user that the program will write the inputs to the file.
puts "I am going to write these to the file"

# Write line1, line2, and line3 to the file each on their own line.
target.write("#{line1}\n#{line2}\n#{line3}\n")

# Inform the user of the final step.
puts "And finally, we close it."
# Close the target file with the close method.
target.close
