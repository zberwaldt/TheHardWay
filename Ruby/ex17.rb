# Declare variables from_file, to_file and assign ARGV to them.
from_file, to_file = ARGV

# Inform user that the program will be copying from_file into to_file.
puts "Copying from #{from_file} to #{to_file}."

# We could do these two on one line, how?
# Declare in_file and assign open(from_file) to it.
in_file = open(from_file)
# Declare indata and assign the data from in_file to it.
indata = in_file.read

# Inform the user the length of the input file.
puts "The input file is #{indata.length} bytes long."

# Put string to query the existence of the file with string formatted answer.
puts "Does the output file exist? #{File.exist?(to_file)}"
# Inform user of readiness, and list actions to take.
puts "Ready, hit RETURN to continue, CTRL-C to abort."
# Get user input
$stdin.gets

# Declare out_file and assign the to_file to it in write mode.
out_file = open(to_file, 'w')
# Write indata to the out_file.
out_file.write(indata)

# Inform user that the task is complete.
puts "Alright, all done."

# Close files
out_file.close
in_file.close
