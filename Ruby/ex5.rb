# Declare my variables.

name = 'Zed A. Shaw'
age = 35 # not a lie in 2009
height = 74 # inches
weight = 180 # lbs
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'
lbs_kg_ratio = 0.452592
in_cm_ratio = 2.54
weight_in_kg = weight * lbs_kg_ratio
height_in_cm = height * in_cm_ratio
# Print variables in relevent strings.

puts "Let's talk about #{name}."
puts "He's #{height} inches tall."
puts "He's #{weight} pounds heavy."
puts "Actually that's not too heavy."
puts "He's got #{eyes} eyes and #{hair} hair."
puts "His teeth are usually #{teeth} depending on the coffee."

# this line is tricky, try to get it exactly right
puts "If I add #{age}, #{height}, and #{weight} I get #{age + height + weight}."
puts "His weight in Kilograms is: #{weight_in_kg} kg."
puts "His height in Centimeters is: #{height_in_cm} cm."
