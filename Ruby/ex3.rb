# + plus
# - minus
# / slash
# * asterisk
# % percent
# < less-than
# > greater-than
# <= less-than-equal
# >= greater-than-equal

# This prints the string "I will now count my chickens:"
puts "I will now count my chickens:"

# Below prints out the number of Hens and Roosters I have.
puts "Hens #{25.0 + 30.0 / 6.0}"
puts "Roosters #{100.0 - 25.0 * 3.0 % 4.0}"

# I am now going to count the number of eggs I have.
puts "Now I will count the eggs:"

# This is the amount of eggs I have.
puts 3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0

# Query: is the sum of the first two values is less than the second pair
puts "Is it true that 3.0 + 2.0 < 5.0 - 7.0?"

# Answer to above query
puts 3.0 + 2.0 < 5.0 - 7.0

# These are a shorthand version of above lines except it is outputting the sum
puts "What is 3.0 + 2.0? #{3.0 + 2.0}"
puts "What is 5.0 - 7.0? #{5.0 - 7.0}"

puts "Oh, what's why it's false."

puts "How about some more."

puts "Is it greater? #{5.0 > -2.0}"
puts "Is it greater or equal? #{5.0 >= -2.0}"
puts "Is it less or equal? #{5.0 <= -2.0}"
