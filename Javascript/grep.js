const cli = require('command-line-args');
const clu = require('command-line-usage');
const glob = require('glob');
const fs = require('fs');

const optionDefinitions = [
    {name: 'pattern', alias: 'p', type: String},
    {name: 'file', alias: 'f', type: String}
];

const options = cli(optionDefinitions);
const nameGlob = `${options.file}`;
const re = new RegExp(options.pattern);

glob(nameGlob, {cwd: '.'}, (err, files) => {
    if (err) throw err;
    for(let file of files) {
        console.log(file);
        console.log("<=======>");
        fs.readFileSync(file).toString().split('\n').forEach((line, i) => {
            // console.log(`${i + 1}: ${line}`);
            if(re.test(line)) {
                console.log(`${i + 1}: ${line}`);
            } else {
                return;
            }
        });
        console.log("\n\n");
    }
});

