#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const chalk = require('chalk');

const optionDefinitions = [
    { name: 'help', alias: 'h', type: Boolean },
    { name: 'in', type: String, defaultOption: true }
];

const sections = [
    {
        header: "Cesar's Cipher",
        content: "Encrypt or decrypt strings"
    }, {
        header: 'Options',
        optionList: [
            {
                name: 'in',
                typeLabel: '{underline file}',
                description: 'The input string. {underline must be surrounded in quotes.}'
            }, {
                name: 'help',
                description: 'Print the usage guide.'
            }
        ]
    }
];

const options = commandLineArgs(optionDefinitions);
const usage = commandLineUsage(sections);

const rot13 = (str) => {
    
    // internal helper funcitons:
    // More succinct name for String.fromCharCode();
    let codeToStr = code => String.fromCharCode(code);
    
    // calculation function for character code
    const rot13Calc = (code) => {
        if (code < 65 || code > 90) {
            return 0;
        } else if(code < 78) {
            return 13;
        } else {
            return -13;
        }
    }

    return str.toUpperCase()
              .split('')
              .map(c => c.charCodeAt())
              .map((c) => codeToStr(c + rot13Calc(c)))
              .join('');
              
};



if (options.help) {
    console.log(usage);
} else if(options.in) {
    console.log(rot13(options.in));
} else {
    console.log(chalk.red("You didn't provide anything."));
}