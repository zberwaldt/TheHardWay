const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const fs = require('fs');

const optionDefinitions = [
    {name: 'help', alias: 'h', type: Boolean, description: 'print out help'},
    {name: 'dir', type: String, description: "directory to search"},
    {name: 'name', type: String, description: "provide a pattern to search for"}
];

const options = commandLineArgs(optionDefinitions);

if(options.help) {
    const usage = commandLineUsage([
        {
            header: 'Typical Example',
            content: 'A simple example demonstration.'
        }, {
            header: 'Options',
            optionsList: optionDefinitions
        }, {
            content: 'Project home: {underline https://github.com/me/example}'
        }
    ]);
    console.log(usage);
} else {
    console.log(options);
    let re;
    if(options.name) {
        re = new RegExp(options.name);
    }
    let files = fs.readdirSync('.', (err, files) => {
        if (err) return console.log(`Unable to scan directory: ${err}`);
        console.log(files);
        return files;
    });
    let matches = files.filter(item => {
        return item.match(re);
    });
    console.log(matches);
}