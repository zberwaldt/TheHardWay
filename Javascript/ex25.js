const counter = (i, upto) => {
    if(i < upto) {
        console.log(`counter: ${i} ${upto}`);
        counter(i + 1, upto);
    }
};

counter(1, 5);

const cbCounter = (i, upto, cb) => {
    if(i < upto) {
        cb(i, upto);
        cbCounter(i + 1, upto, cb);
    }
};

cbCounter(1, 6, (i, j) => {
    console.log(i, j);
});