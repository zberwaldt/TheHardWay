const var_sucks = () => {
    var varScope = 10;
    let letScope = 20;
  
    console.log(`>>> var_sucks before if: var=${varScope}, let=${letScope}`);
  
    if(true) {
      var varScope = 100;
      let letScope = 100;
      console.log(`>>> var_sucks inside if: var=${varScope}, let=${letScope}`);
    }
  
    console.log(`>>> var_sucks after if: var=${varScope}, let=${letScope}`);
  };
  
  var_sucks();

  // puzzle: how small can you make this?

const build_adder = (left) => {
    // do I really need this variable?
    let left_hand = left;
    return (right) =>  {
      // do I really need the return?
      let result = left_hand + right;
      return result;
    };
  };
  
  let add10 = build_adder(10);
  let add20 = build_adder(20);
  
  console.log(`test builder 3 + 10 == ${add10(3)}`);
  console.log(`test builder 3 + 20 == ${add20(3)}`);
  console.log(`test builder 13 + 10 == ${add10(13)}`);
  console.log(`test builder 3 + 10 + 20 == ${add10(add20(3))}`);