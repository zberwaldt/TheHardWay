const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const fs = require('fs');

const optionDefinitions = [
    { name: 'help', alias: 'h', type: Boolean, description: '' },
    { name: 'src', type: String, multiple: true, defaultOptions: true },
    { name: 'out', type: String }
];

const options = commandLineArgs(optionDefinitions);

if(options.help) {
    const usage = commandLineUsage([
        {
            header: 'Typical Example',
            content: 'A simple example demonstration.'
        }, {
            header: 'Options',
            optionsList: optionDefinitions
        }, {
            content: 'Project home: {underline https://github.com/me/example}'
        }
    ]);
    console.log(usage);
} else {
    if(options.src) {
        for(let src of options.src) {
            fs.readFile(src, (err, data) => {
                if(err) console.log(err);
                console.log(data.toString());
            });
        }
    }
}
