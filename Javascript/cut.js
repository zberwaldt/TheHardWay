// Cut Removes Strings from files.
// use glob to find files
// takes a string to 
const cli = require('command-line-args');
const clu = require('command-line-usage');
const chalk = require('chalk');
const glob = require('glob');
const fs = require('fs');

// define options for command
const optionDefinitions = [
    // must be able to specify multiple ranges for character
    { name: 'file', type: String, defaultOption: true },
    { name: 'character', alias: 'c', multiple: true, type: String },
    { name: 'delimiter', alias: 'd', type: String},
    { name: 'field', alias: 'f', type: String},
    { name: 'help', alias: 'h', type: Boolean}
];

// define documentations for cut
const sections = [
    {
        header: "cut",
        content: "print selected parts of lines from files to stdout. A javascript implementation of the Unix cut command."
    }, {
        header: "Options",
        optionList: [
            {
                name: 'file',
                typeLabel: '{underline FILE}',
                description: 'the file(s) you want to search.'
            },{
                name: 'character',
                alias: 'c',
                typeLabel: '{underline TEXT}',
                description: 'the regex pattern you want to search files for and remove.'
            },{
                name: 'delimiter',
                alias: 'd',
                typeLabel: '{underline TEXT}' 
            },{
                name: 'help',
                description: 'Print the usage guide.'
            }
        ]
    }
];

const options = cli(optionDefinitions);
const usage = clu(sections);
const nameGlob = `${options.file}`;
const character = options.character;
const delimiter = options.delimiter;
const field = parseInt(options.field,10);  // field and elimiter is used together, delimiter default should be tab, according to man of cut

const parseArgs = (range) => {
    let dashRegex = /-/;

    let parsedArg;
    let rangeStart;
    let rangeEnd;

    if (dashRegex.test(range)) {
        // if there is a dash
        // process the range
        let rangeRegex = /(\d+)*-?(\d+)*/;
        parsedArg = rangeRegex.exec(range);
        if(parseInt(parsedArg[1], 10)) {
            rangeStart = parseInt(parsedArg[1], 10);
        }

        if(parseInt(parsedArg[2], 10)) {
            rangeEnd = parseInt(parsedArg[2], 10);
        }

        return { 
            start: rangeStart, 
            end: rangeEnd 
        };

    } else {
        // otherwise just use the value
        return {start: parseInt(character, 10)};
    }

};

if(options.help) {
    console.log(usage);
} else {
    // TODO: Cut command must have one of the following: -b, -c, -f
    // * must exit if none given
    if(character) {

        let charArgs = [];

        for (let range of character) {
            charArgs.push(parseArgs(range));
        }
        
        console.log(charArgs);

        // Find all files that match the glob pattern
        glob(nameGlob, {cwd: '.'}, (err, files) => {
            if(err) throw err;
        
            // check if no files are found.
            if (files.length === 0) {
                console.error(chalk.red("No files found."));
                process.exit(1);
            }
        
            // for each file, move through each line in the file
            for (let file of files) {
                try {
                    fs.readFileSync(file).toString().split('\n').forEach((line, i) => {
                        let output = [];
                        for(let range of charArgs) {
                            if(range.hasOwnProperty('end')) {
                                output.push(line.slice(range.start, range.end));
                            } else {
                                output.push(line.charAt(range.start));
                            }
                        }
                        console.log(output.join(''));
                    });
                } 
                catch(error) {
                    console.log(error);
                }
            }
            process.exit(1);
        });
    } else if (delimiter && field) {
        // delimiter and field must be used in conjunction
        // TODO: implement delimiter
        // string split by delimiter
        
        glob(nameGlob, {cwd: '.'}, (err, files) => {
            if(err) throw err;
        
            // check if no files are found.
            if (files.length === 0) {
                console.error(chalk.red("No files found."));
                process.exit(1);
            }
            
            // for each file, move through each line in the file
            for (let file of files) {
                try {
                    fs.readFileSync(file).toString().split('\n').forEach((line, i) => {
                        let shiftedField = field - 1;
                        console.log(line.split(delimiter)[shiftedField]);
                    });
                } 
                catch(error) {
                    console.log(error);
                }
            }
        });
    } else if (field) {
        // TODO: fold field into delimiter.
    } else {
        console.log('You must have one of the following options: --field, --delimiter, or --character');
        process.exit();
    }
    // define a regex for parsing numbers

    // extract the numbers from the range
    

    
        // find the the matches
        // remove them from the files.
        // modify the files in place.
    

    // TODO: Roll glob into function (?)
    // searchFiles(filePattern, cwd, cutOption)

    // TODO: If no file provided user STDIN instead.
    // probably the readline library for this.
}

