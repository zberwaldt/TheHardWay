const readline = require('readline-sync');

class Game {
    constructor (rooms) {
        this.hp = Math.floor(Math.random() * 10) + 1;
        this.rooms = {};
        for(let room of rooms) {
            this.rooms[room.name] = room;
        }
    }

    say(prompt) {
        console.log(prompt);
    }

    die(message) {
        this.say(`!!!!!!!!!!!!!!!!!!!!!\n\n${message}`);
        process.exit(1);
    }

    ask(prompt) {
        console.log(`[[You have ${this.hp} hit points.]]`);
        if(this.hp <= 0) {
            this.die("You died!");
        } else {
            let x = readline.question(prompt + ' ');
            return x;
        }
    }

    go(where) {
        let room = this.rooms[where];
        return room.enter(this);
    }

    play(name) {
        let nextRoom = this.go(name);
        while(nextRoom) {
            nextRoom = this.go(nextRoom);
        }
    }

    hit(amount) {
        this.hp -= amount;
        if(this.hp <= 0) {
            this.die("You died from internal injuries.");
        }
    }
}

class Room {
    constructor(name) {
        this.name = name;
    }

    enter(game) {
        console.log("Implement me!");
    }
}

class Door extends Room {
    enter(game) {

        game.say("Walking through the door, you enter what looks like a vault. There is a computer with a password entered in, which you think is extermely irresponsible. You contemplate entering the CODE or SMASH the computer");

        let next = game.ask("What do you do?");
        
        if(next === "code") {
            return "gold";
        } else if (next === "smash" ) {
            game.die("You've triggered an explosive, and die a firey death");
        } else {
            return "door";
        }
      // they have to open the door to get the gold
      // what kind of puzzle will they solve?
    }
}

class Spider extends Room {

  enter(game) {
    game.say("You get around the bend and encounter a giant ass spider! It takes a swipe at you with one of it's legs.");
    game.hit(10);
    game.say("You've somehow survived and run back down the tunnel.");
    return "well";
    // they enter here, and the spider takes 10 hit points
    // if they live then they can run away
  }

}

class Gold extends Room {

  enter(game) {
    
    game.say("Entering the room, you witness a large pile of gold. You're rich, and will finally be able to pay off your student loans from college");
    
    game.say("You've won the game of life.");
    
    process.exit(1);
    // end of the game they win if they get the gold
  }

}

class Rope extends Room {

  enter(game) {
    
    game.say("You are at the bottom of the well.");
    
    game.say("It's pretty dark but you are obviously in a tunnel. To your left the tunnel goes around a bend. To your right there is a door.");
    
    let next = game.ask("What do you do?");
    console.log(next);
    if(next === "right") {
        game.say("You open the door, and walk through.");
        return "door";
    } else if (next === "left") {
        game.say("You head down the tunnel and around the bend...");
        return "spider";
    } else {
        console.log("You can't do that.");
        return "rope";
    }
  }

}

class Well extends Room {

  enter(game) {

    game.say("You are walking through the woods and see a well. Walking up to it and looking down you see a shiny thing at the bottom.");

    let next = game.ask("What do you do?");

    if(next === "climb") {
        game.say("You climb down the rope.");
        return "rope";
    } else if(next === "jump") {
        game.say("Yikes! Let's see if you survive!");
        game.hit(5);
        return "rope";
    } else {
        game.say("You can't do that here.");
        return "well";
    }
  }

}



let rooms = [
    new Well("well"), 
    new Rope("rope"),
    new Gold("gold"),
    new Spider("spider"),
    new Door("door")
];
let game = new Game(rooms);
game.play("well");