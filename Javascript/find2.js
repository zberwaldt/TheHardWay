const cli = require('command-line-args');
const glob = require('glob');

const options_def = [
    {name: 'root', type: String, multiple: true, defaultOption: true},
    {name: 'name', type: String, multiple: true},
    {name: 'print', type: Boolean},
];

const options = cli(options_def);
let name_glob = `**/${options.name}`;

glob(name_glob, {cwd: options.root[0]},
   (er, files) => {
       if(options.print) {
        console.log(files);
       }
   }
);