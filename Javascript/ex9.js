const fs = require('fs');

let contents = fs.readFileSync("test.txt");

console.log("Contents:");
console.log(contents.toString());

console.log("----------------");

fs.readFile("test.txt", (err,data) => { // make sure you type file correnctly
    if(err) console.log(err);
    console.log(data.toString());
});