// You can give multiple things for console.log to print.
// Separate each thing with a comma.
console.log("Name:", "Zach Berwaldt");
console.log("Age:", 29);
console.log("Feet Tall:", Math.round(75/12));
console.log("And Inches:", 75 - (Math.round(75 / 12) * 12));
console.log("Age * Height:", 29 * 75);
console.log("Age * Feet:", 43 * Math.round(75 /12));