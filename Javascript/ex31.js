const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');

const optionDefinitions = [
    { name: 'help', alias: 'h', type: Boolean, description: 'Display this usage guide.'},
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'src', type: String, multiple: true, defaultOptions: true },
    { name: 'timeout', alias: 't', type: Number }
];

const options = commandLineArgs(optionDefinitions);

if(options.help) {
    const usage = commandLineUsage([
        {
            header: 'Typical Example',
            content: 'A simple example demonstrating typical usage.'
        },{
            header: 'Options',
            optionsList: optionDefinitions
        },{
            content: 'Project home: {underline https://github.com/me/example}'
        }
    ]);
    console.log(usage);
} else {
    console.log(options);
}
    