import click 

@click.group()
def greet():
    pass

@greet.command()
@click.argument('name')
@click.option('-c', required=True)
def file(**kwargs):
    column_number = int(kwargs['c'])
    with open(kwargs['name']) as file:
        for line in file.readlines():
            print(line[:column_number])

if __name__ == '__main__':
    greet()