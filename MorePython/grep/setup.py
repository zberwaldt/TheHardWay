try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Python grep implementation',
    'author': 'Zach Berwaldt',
    'author_email': 'zberwaldt@tutamail.com',
    'version': '0.1',
    'py_modules': ['grep'],
    'install_requires': ['Click'],
    'entry_points': '''
        [console_scripts]
        grep=grep:cli
    ''',
    'name': 'grep'
}

setup(**config)