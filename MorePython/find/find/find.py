import click
from glob import glob

@click.command()
@click.argument('root', default='.')
@click.option('-F', '--file', help="File glob")
@click.option('-R', '--recursive', is_flag=True, help="enable recursive search")
def cli(**kwargs):
    """Find files in a directory hierarchy"""
       
    if kwargs['recursive']:
        files = glob(f"{kwargs['file']}", recursive=True)
    else:
        files = glob(f"{kwargs['file']}")

    if len(files) < 1:
        click.echo("No files found")
        exit(1)
    else:
        for file in files:
            click.echo(file)