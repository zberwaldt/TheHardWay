try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'A python implementation of the Unix find command.',
    'author': 'Zach Berwaldt',
    'author_email': 'zberwaldt@tutamail.com',
    'version': '0.1',
    'install_requires': ['Click'],
    'packages': ['find'],
    'py_modules': ['find'],
    'entry_points': '''
        [console_scripts]
        find=find.find:cli
    ''',
    'name': 'find'
}

setup(**config)