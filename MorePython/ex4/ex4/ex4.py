from sys import argv,stdin, stdout
import re
import argparse

def parse_argv(argv):
    return argv

parser = argparse.ArgumentParser(description='Copy Test')

parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=stdin)
parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=stdout)

args = parser.parse_args()
