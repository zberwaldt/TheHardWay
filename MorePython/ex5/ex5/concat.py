import click

def greeter(**kwargs):
    output = '{0}, {1}!'.format(kwargs['greeting'], kwargs['name'])

    if kwargs['caps']:
        output = output.upper()
    print(output)

@click.group()
def greet():
    pass


@greet.command()
@click.argument('name')
@click.option('--linenumbers', is_flag=True)
def file(**kwargs):
    with open(kwargs['name']) as file:
        if kwargs['linenumbers']:
            for linenumber, line in enumerate(file, 1):
                print(linenumber, line)
        else:
            print(file.read())


if __name__ == '__main__':
    greet()