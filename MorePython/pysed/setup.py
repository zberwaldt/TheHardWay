try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Python implementation of Unix sed command',
    'author': 'Zach Berwaldt',
    'author_email': 'zberwaldt@tutamail.com',
    'version': '0.1',
    'install_requires': ['Click', 'pytest'],
    'packages': ['pysed'],
    'py_modules': ['pysed'],
    'entry_points': '''
        [console_scripts]
        pysed=pysed.pysed:cli
    ''',
    'name': 'pysed'
}

setup(**config)