from sys import exit
from time import sleep
import random

player = {
    'location': 'bed',
    'awake': False,
    'inventory': []
}

def dead(message):
    print(message, "Good Job.")
    exit(0)

def start():
    print('''
        It's 2am. You've just woken up.
        You don't know why.
        Do you STAY IN BED or GET UP?
    ''')

    random_response = [
        "What?",
        "Come again?",
        "...",
        "Stop that.",
        "Quit it.",
        "Please answer."
    ]

    while True: 
        choice = input("> ")
        if choice.lower() == "stay in bed":
            dead("You eventually fall asleep.")
        elif choice.lower() == "get up":
            your_room()
        else:
            print(random_response[random.randint(0, len(random_response) - 1)])
            sleep(0.5)
            print("Do you STAY IN BED or GET UP?")

def your_room():

    if player['location'] == 'bed':
        print('''
        You look around you. It's your bedroom.
        Your DRESSER is across the room in front of you.
        The DOOR out is in the corner on your left.
        Your BED is behind you.
        Do you OPEN YOUR DRESSER or LEAVE YOUR ROOM or GO BACK TO BED?
        ''')
    elif player['location'] == 'closet':
        print('''
            You've returned to the center of the room.
            Your DRESSER is across the room in front of you.
            The DOOR out is in the corner on your left.
            Your BED is behind you.
            Do you OPEN YOUR DRESSER or LEAVE YOUR ROOM or GO BACK TO BED?
        ''')
    
    random_response = [
        "What?",
        "Come again?",
        "...",
        "Stop that.",
        "Quit it.",
        "Please answer."
    ]

    player['location'] = 'room'

    while True:
        choice = input("> ")
        if choice.lower() == "open your dresser" or choice.lower() == "dresser" or choice.lower() == "open":
            dead("You open the dressor, but for some reason the door open at the speed of light, and your head is cut off.")
        elif choice.lower() == "leave" or choice.lower() == "door" or choice.lower() == "leave room" or choice.lower() == "leave your room":
            dead("You walk out right as a nuclear bomb goes off, and you are incinerated.")
        else:
            print(random_response[random.randint(0, len(random_response) - 1)])
            sleep(0.5)
            print("Do you OPEN YOUR DRESSER or LEAVE YOUR ROOM?")

def the_closet():
    print('''
        You open the closet. There is a lone pair of PANTS in it.
        Do you PUT ON THE PANTS or STEP AWAY?
    ''')

    random_response = [
        "What?",
        "Come again?",
        "...",
        "Stop that.",
        "Quit it.",
        "Please answer."
    ]
    
    while True:
        choice = input("> ")
        if choice.lower() == "put on the pants" or choice.lower() == "put on" or choice.lower() == "put on" or choice.lower() == "pants":
            dead("There was a rattle snake in there and he bit you...right on the balls. So you died.")
        elif choice.lower() == "step away":
            your_room()
        else:
            print(random_response[random.randint(0, len(random_response) - 1)])
            sleep(0.5)
            print("Do you PUT ON THE PANTS or STEP AWAY?")
        
        

start()