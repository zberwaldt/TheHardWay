# ex12.py is just a alternative version of ex11.py, but with more efficient use of the input function

age = input("How old are you? ")
height = input("How tall are you? ")
weight = input("How much do you weight? ")

print(f"So, you're {age} old, {height} tall and {weight} heavy.")