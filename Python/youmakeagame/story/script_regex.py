import re

file = open('idealscript.txt')
txt = file.read()
# split file into a list of "paragraphs" or by double \n

story = []
action_tags = []
paragraphs = re.split(r"\n\n", txt)

def strip_action_tags(paragraph):
    
    open_tag_regex = re.compile(r"\[\w+\]", re.I|re.M)
    close_tag_regex = re.compile(r"\[\/\w+\]", re.I|re.M)
    
    if close_tag_regex.search(paragraph) is None:
        ct = None
    else:
        ct = close_tag_regex.search(paragraph).group(0)
    
    if open_tag_regex.search(paragraph) is None:
        ot = None
    else:
        ot =open_tag_regex.search(paragraph).group(0)
        
    paragraph = re.sub(open_tag_regex,'', paragraph) # remove the opening tag
    paragraph = re.sub(close_tag_regex, '', paragraph) # remove the closing tag

    return (ot,ct), paragraph

for paragraph in paragraphs:
    strip_action_tags(paragraph)
    print(strip_action_tags(paragraph)[1])
    
    paragraph = paragraph.strip('\n') # remove trailing \n from paragraph
    parsed_paras.append(paragraph) # add to list of cleaned paragraphs

# for paras in parsed_paras:
#     print("<BEGIN PARA>\n", paras, "\n<END PARA>")



file.close()