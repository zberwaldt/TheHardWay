class Lifeform(object):

    """
    This is the base class for all lifeforms in my game. 
    """

    def __init__(self, first_name="John", last_name="Smith", health=100):
        self.first_name = first_name
        self.last_name = last_name
        self.health = health

    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"