from scenes import broom_closet, death, finished

"""
Rename this file to something else. Please.
"""


class Map(object):

    def __init__(self, start_scene, scene_dict=None):
        self.start_scene = start_scene
        if type(scene_dict) is not dict:
            raise ParameterError("scene_dict must be a dictionary of scenes.")
        else:
            if scene_dict is not None:
                self.scene_dict = scene_dict
            else:
                self.scene_dict = self.generate()

    def generate(self):
        """ 
        Generate a random map.
        Currently not implemented.
        """
        new_map = {}
        return new_map

    def next_scene(self, scene_name):
        val = self.scene_dict.get(scene_name)
        return val

    def opening_scene(self):
        return self.next_scene(self.start_scene)


class ParameterError(Exception):
    pass


