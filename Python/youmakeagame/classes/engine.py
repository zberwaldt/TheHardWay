# A shameless exact copy of the engine from Gothons from Planet Percal #25
class Engine(object):
    """
    This class is the game engine, it runs in a loop until 
    the appropriate condition is met and the game ends
    """
    
    def __init__(self, game_map):
        self.game_map = game_map
        
    def play(self):
        # define the current scene, which is ini
        current_scene = self.game_map.opening_scene()
        # define the end point of the game
        last_scene = self.game_map.next_scene('finished')

        while current_scene != last_scene:

            # get the next scene name by running through the logic of the enter method
            # of whatever is the current_scene.
            next_scene_name = current_scene.enter()

            # update the current scene, so the game progresses
            current_scene = self.game_map.next_scene(next_scene_name)
        
        # insure that the last scene gets played.
        current_scene.enter()