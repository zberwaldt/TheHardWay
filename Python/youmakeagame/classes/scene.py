import os
import sys
import re

class Scene(object):

    """
    EXPLANATION: I wanted to be able to create my story seperate of my game as txt docs. Then I could use each scene subclass to go find it's own "script"
    and parse it all on it's own. That way each scene could develop and change (in theory) depending on the users choices. Maybe this is a really bad idea. I'm sure there is a much smarter and safer way to do this (if it's even unsafe)
    """

    def __init__(self, hints=None):
        components = self.process_script(self.__class__.__name__)
        self.hints = components.actions
        self.story = components.tale
    
    def enter(self):
        """
        I'm leaving this alone, so I can make custom implementations on a scene by scene basis. I think this is probably the only way to do it. maybe when I know more I can think of an implementation to abstract a loop into the scene base class.
        """
        print("This scene is not yet configured.")
        
        print("Subclass it and implement enter()>")
        
        exit(1)
    
    def ponder_options(self):
        """
        rather than ask for user input all the time, heres a method to do that.
        """
        print("You ponder your options...")

        while True:
            action = input('> ')    
            
            if action != "hint":
                return action
            else:
                self.get_hint()
    
    def get_hint(self):
        """
        Hint system. this will be handy later in the book when I do the lexicon stuff. For now, it's just a crude implementation with author provided hints. 
        """
        print("Here are the actions you can perform:")
        for h in self.hints:
            print(h)
    
    # ------- Depreciated -------
    # def parse_script(self, script):
    #     """
    #     The idea for this method would be to take a .txt file and parse it based on my own pre-concieved format (like split by paragraph, which I think I need regex for.) Then in the enter method it will loop through each stage of a scene based on user input. 
    #     """
    #     txt = open(f'./story/{script}.txt', 'r')
    #     parsed_script = txt.read().split('**')
    #     txt.close()
    #     return parsed_script
    
    def process_script(self, script):
        """
        This method is responsible for processing the script file that matches the particular scene name, and break it down to it's components: The story and the actions possible. which get passed onto for furth processing (if need be)
        """
        # open the file, don't for get to close it
        file = open(f'./story/{script}.txt', 'r') 
        txt = file.read()
        file.close()
        
        # predefined regex patterns
        paragraphs = re.split(r"\n\n", txt)
        
        # create lists for each component of a script
        acts = [] # acts you can perform
        chapters = [] # For the purposes of this project
        
        for paragraph in paragraphs:
            result = self.strip_action_tags(paragraph)
            chapters.append(result[0])
            acts.append(result[1])

        story = Story(chapters, acts)

        return story

    def strip_action_tags(self, paragraph):
       
        open_tag_regex = re.compile(r"\[\w+\]", re.I|re.M)
        close_tag_regex = re.compile(r"\[\/\w+\]", re.I|re.M)
        
        if close_tag_regex.search(paragraph) is None:
            ct = None
        else:
            ct = close_tag_regex.search(paragraph).group(0)
        
        if open_tag_regex.search(paragraph) is None:
            ot = None
        else:
            ot =open_tag_regex.search(paragraph).group(0)
                       
        paragraph = re.sub(open_tag_regex,'', paragraph) # remove the opening tag
        paragraph = re.sub(close_tag_regex, '', paragraph) # remove the closing tag
        paragraph.strip('\n') # remove trailing \n from paragraph

        return paragraph, (ot, ct)

""" 
12:03am 8/14/2018 => I just realized If I learn regex I could potentially write my txt scripts in a sort of xml or html type format and parse stages of a story based of that. Then I could create a possible_actions_parser, based off my tags.
"""           

class SceneParameterError(Exception):
    pass

class Story(object):
    def __init__(self, story, actions):
        self.tale = story
        self.actions = self.parse_actions(actions)
    
    def parse_actions(self, actions):
        action_list = []
        action_re = re.compile(r'\w+')
        for action in actions:
            if None not in action:
                val = action[0]
                if val not in action_list:
                    action_list.append(action_re.search(val).group(0))
        return action_list

            