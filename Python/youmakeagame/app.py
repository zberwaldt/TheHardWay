from classes.engine import Engine
from classes.map import Map
from scenes import broom_closet, death, finished, bedroom
from scenes import street, grocery_store


"""
This is the main script of ex45. where everything is brought together and run.
"""
scene_dict = {
    'broom_closet': broom_closet.BroomCloset(),
    'bedroom': bedroom.Bedroom(),
    'street': street.Street(),
    'grocery_store': grocery_store.GroceryStore(),
    'death': death.Death(),
    'finished': finished.Finished()
}

# added this to prevent interferance with pytest 

if __name__ == "__main__":
    a_map = Map('broom_closet', scene_dict)
    a_game = Engine(a_map)
    a_game.play()    
