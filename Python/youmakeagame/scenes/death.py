from classes import scene

class Death(scene.Scene):
    
    def enter(self):
        
        print("You died.")
        
        exit(1)