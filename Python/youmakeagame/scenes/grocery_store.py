from classes import scene
from textwrap import dedent

class GroceryStore(scene.Scene):

    def enter(self):
        stage = 0
        print(self.story[stage])
        action = self.ponder_options()
        while True: 
            if action == "purchase":
                stage += 1
                print(self.story[stage])
                return 'finished'
            else:
                print('The doors close on you and you are crushed.')
                return 'death'