from classes import scene

from textwrap import dedent

class BroomCloset(scene.Scene):
    """
    This is the broom closet. 
    """
    def enter(self):
        stage = 0
        while True:
            print(self.story[stage])
            action = self.ponder_options()
            if action == 'leave':
                stage += 1
                print(self.story[1])
                return 'bedroom'
        