from classes import scene
from textwrap import dedent

class Finished(scene.Scene):
    
    def enter(self):
        print(dedent("""You've become an accomplished adult. Congratulations.        """))

        return "finished"