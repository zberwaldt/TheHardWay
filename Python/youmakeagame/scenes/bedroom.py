from classes import scene
from textwrap import dedent

class Bedroom(scene.Scene):      
    clean = False
    def enter(self):
        stage = 0
        print(self.story[stage])
        while True:
            action = self.ponder_options()
            if action not in self.hints:
                print("Come again?")
            else:
                if action == "clean" and not Bedroom.clean:
                    stage = 1
                    print(self.story[stage])
                    Bedroom.clean = True
                else:
                    print("The room can't be cleaned anymore.")
                
                if action == "get":
                    stage = 2
                    print(self.story[stage])
                    
                    return 'street'
            
            

   

