from classes import scene
from textwrap import dedent

class Street(scene.Scene):
    
    def enter(self):
        stage = 0    
        print(self.story[stage])
        action = self.ponder_options()

        if action == 'check':
            stage = 1
            print(self.story[stage])
            return 'death'
            
        elif action == 'cross':
            stage = 2
            print(self.story[stage])
            return 'grocery_store'

        