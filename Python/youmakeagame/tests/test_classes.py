import pytest

import builtins

from classes import map, lifeform, engine, scene

from scenes import broom_closet, death, finished, bedroom

import app


def setup():
    print("SETUP!")

def test_map():
    
    new_map = map.Map('broom_closet', {'broom_closet': broom_closet.BroomCloset('broom_closet')})
    
    assert new_map.start_scene == 'broom_closet'
        
    assert type(new_map.scene_dict['broom_closet']) == broom_closet.BroomCloset
    
    with pytest.raises(map.ParameterError):
        map.Map('broom_closet', [])

def test_scenes_exception():
    test_scene = scene.Scene()

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        test_scene.enter() 

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1

def test_scene_hint(capsys):
    test_scene = scene.Scene()
    test_scene.get_hint()
    out = capsys.readouterr()
    assert out.out == 'Here are the actions you can perform:\n'

def test_start():
    assert type(app.scene_dict) is dict

    assert type(app.scene_dict['broom_closet']) == broom_closet.BroomCloset

def test_scene_methods(capsys):
    new_scene = scene.Scene()
    assert type(new_scene.story) == list

def test_script_parser():
    new_scene = scene.Scene()
    assert type(new_scene.process_script('idealscript')) == scene.Story
    assert type(new_scene.process_script('idealscript').tale) == list
    assert type(new_scene.process_script('idealscript').actions) == list

def test_tag_strip():
    new_scene = scene.Scene()
    result = new_scene.strip_action_tags("""[clean]
    You've elected to clean your room. It'll finally be nice to see the floor
again. Your Mom would be proud that you've arrived at this moment in your
life. 
    After finally cleaning the room you notice a piece of paper on the floor. 
You reach down, and pick it up. It's a grocery list. You must need to get
groceries.
[/clean]
    """)
    assert type(result) == tuple 
    assert type(result[0]) is str
    assert result[1] == ("[clean]", "[/clean]")

def teardown():
    print("TEAR DOWN!")

"""
12:07am 8/14/2018 => I really need to work on writing tests first. I think it'll help me stay focused on building from the ground up, instead of getting caught up with 'feature creep' which is a real thing.
"""
