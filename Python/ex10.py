tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

# fat_cat = """
# I'll do a list:
# \t* Cat food
# \t* Fishies
# \t* Catnip\n\t* Grass
# """

# Study Drill 2
fat_cat = '''
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
'''

study_drill = "https:\\\\www.url.com\\Study-Drill-3\\\f"


print(tabby_cat)
print(persian_cat)
print(backslash_cat)
print(fat_cat)
# Study Drill 3
print(study_drill) 