gothons_from_planet_percal = [
    "central_corridor",
    "laser_weapon_armory"
]

zed_shaw_vs_the_world = [
    "florida",
    "train_station"
]

maps = {
    "Gothons from Planet Percal #25": gothons_from_planet_percal,
    "Zed Shaw vs. League of Terrible Developers": zed_shaw_vs_the_world
}