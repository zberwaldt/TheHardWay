class Room(object):
    def __init__(self, name, description, attempts=None):
        self.name = name
        self.description = description
        self.paths = {}
        self.hints = {}
        self.attempts = attempts

    def go(self, direction):
        return self.paths.get(direction, None)

    def update_paths(self, paths):
        self.paths.update(paths)

    def update_help(self, hint):
        self.hints.update(hint)

    def send_help(self):
        return self.hints

class Planisphere(object):
    
    def __init__(self, name):
        self.name = name
        self.rooms = {}
        self.start_scene = None
        self.__whitelist = None

    
    def update_rooms(self, room):
        self.rooms.update(room)

    def next_scene(self, scene_name):
        val = self.rooms.get(scene_name)
        return val

    def opening_scene(self):
        return self.next_scene(self.start_scene)
    