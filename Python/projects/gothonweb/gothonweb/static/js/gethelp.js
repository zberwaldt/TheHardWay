(function() {
    const helpBtn = document.querySelector('#getHelp');

    function createHelp(hints) {
        if (!document.querySelector('#help')) {
            let div = document.createElement('div');
            let helpTitle = document.createElement('h4');
            let hintList = document.createElement('ul');
            let closeBtn = document.createElement('button');

            div.setAttribute('id', 'help');
            div.setAttribute('class', 'fadeIn');
            helpTitle.textContent = "Maybe you could:";
            hintList.setAttribute('id', 'hints');
            closeBtn.textContent = "Close";
            closeBtn.addEventListener('click', (e) => {
                div.remove();
            });

            div.appendChild(helpTitle);
        
            for(let hint in hints) {
                let hintListItem = document.createElement('li');
                hintListItem.textContent = hint;
                hintList.appendChild(hintListItem);
            }
            
            div.appendChild(hintList);
            div.appendChild(closeBtn);
            document.querySelector('body').appendChild(div);   
        } else {
            console.warn("Help already exists");
            return;
        }
    }

    helpBtn.addEventListener('click', async (e) => {
        createHelp(await fetchHelp());
    });

    async function fetchHelp() {
        let help;
        await fetch('/help', {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        }).then(res => help = res.json());
        return help;
    }

})();