window.addEventListener('load', function() {
    const target = document.querySelector('#target');
    target.addEventListener('animationend', (e) => {
        fetch('/game', {
            method: 'POST',
            redirect: "follow",
            credentials: 'same-origin'
        }).then(res => {
            window.location.href = res.url;
        });
    });
});

