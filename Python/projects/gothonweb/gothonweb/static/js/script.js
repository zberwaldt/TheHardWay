window.addEventListener('load', (e) => {
    const restartBtn = document.querySelector("#game-restart");
    restartBtn.addEventListener('click', (e) => {
        console.log("click");
        let restartRequest = confirm("Are you sure you want to restart the game?");
        if(restartRequest) {
            return true;
        } else {
            e.preventDefault();
        }
    });

    if(document.querySelector('#actionForm')) {
        const actionForm = document.querySelector('#actionForm');
        const actionInput = document.querySelector('#action-input');
        actionInput.focus();
        actionForm.addEventListener('keyup', (e) => {
            if(e.keycode === 13) {
                actionForm.submit();
            }
        });
    }

});
