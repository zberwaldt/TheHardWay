import pytest
import flask
from app import app

app.config['TESTING'] = True
web = app.test_client()

def setup():
    print("SETUP!")
    
def teardown():
    print("TEAR DOWN!")

def test_index():
    rv = web.get('/', follow_redirects=True)
    assert b'Central Corridor' in rv.data
    assert rv.status_code == 200

def test_help():
    with web as c:
        rv = c.get('/help')
        json_data = rv.get_json()
        print(json_data)
        assert 'dodge!' in json_data

def test_help_fail():
    with web as c:
        rv = c.get('/', follow_redirects=True)
        assert b'Central Corridor' in rv.data
        rv = c.post('/game', data={'action': 'tell a joke'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '0132'}, follow_redirects=True)
        rv = c.post('/game', data={'action': 'slowly place the bomb'}, follow_redirects=True)
        print(rv.data)
        assert b'Escape Pod' in rv.data
        rv = c.get('/help')
        json_data = rv.get_json()
        print(json_data)
        assert not json_data


def test_room_name():
    with web as c:
        c.get('/', follow_redirects=True)
        with c.session_transaction() as sess:
            print(sess)
            assert 'room_name' in sess
            assert sess['room_name'] == 'central_corridor'
            return
        return


def test_game_input():
    with web as c:
        rv = c.get('/', follow_redirects=True)
        rv = c.post('/game', data={'action': 'tell a joke'}, follow_redirects=True)
        assert rv.status_code == 200
        assert b'Laser Weapon Armory' in rv.data
        rv = c.post('/game', data={'action': '0132'}, follow_redirects=True)
        print(rv.data)
        assert b'The Bridge' in rv.data

def test_laser_room_fail():
    with web as c:
        rv = c.get('/', follow_redirects=True)
        rv = c.post('/game', data={'action': 'tell a joke'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '012'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '01'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '012'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '012'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '032'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '02'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '02'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '032'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '02'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '02'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '02'}, follow_redirects=True)
        rv = c.post('/game', data={'action': '02'}, follow_redirects=True)
        print(rv.data)
        assert b'death' in rv.data

def test_bridge_fail():
    with web as c:
        rv = c.get('/', follow_redirects=True)
        rv = c.post('/game', data={'action': 'tell a joke'}, follow_redirects=True)
        rv = c.post('/game', data={'action': 'throw the bomb'}, follow_redirects=True)
        print(rv.data)
        assert b'death' in rv.data
        
        
def test_menu_session():
    with web as c:
        rv = c.get('/menu')
        assert b'Adventures in Text' in rv.data
        assert rv.status_code == 200
        assert flask.session['adventure'] == None

def test_index_post():
    rv = web.post('/game', data=dict(
        action="tell a joke"
    ))
    assert rv.status_code == 302

