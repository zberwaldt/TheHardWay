import gothonweb.planisphere as planisphere
import pytest


def test_room_paths(capsys):
    planisphere.check_room_paths('central_corridor')
    out = capsys.readouterr()
    assert 'shoot!' in out.out

def test_room_paths_fail():
    with pytest.raises(SystemExit) as sysExit:
        planisphere.check_room_paths('generic')
        assert sysExit.type == SystemExit

def test_room_help():
    test = planisphere.load_room_help('central_corridor')
    assert 'shoot!' in test

def test_room_help_fail():
    with pytest.raises(SystemExit) as sysExit:
        planisphere.load_room_help('generic')
        assert sysExit.type == SystemExit

def test_name_room_fail():
    with pytest.raises(SystemExit) as sysExit:
        planisphere.name_room('generic')
        assert sysExit.type == SystemExit

def test_load_room_fail():
    with pytest.raises(SystemExit) as sysExit:
        planisphere.load_room('generic')
        assert sysExit.type == SystemExit