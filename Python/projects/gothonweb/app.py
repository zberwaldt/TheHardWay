from flask import Flask, session, redirect, url_for, escape, request
from flask import render_template, jsonify
from gothonweb import planisphere
from gothonweb.maps import maps

app = Flask(__name__, template_folder='./gothonweb/templates', static_folder='./gothonweb/static')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('errors/404.html')

@app.errorhandler(500)
def internal_error(e):
    return render_template('errors/500.html')

@app.errorhandler(403)
def forbidden(e):
    return render_template('errors/403.html')

@app.route("/")
def index():
    
    #this is used to "setup" the session with starting values
    session['room_name'] = planisphere.START
    session['adventure'] = 'Gothons from Planet Percal #25'
    return redirect(url_for("game"))

@app.route('/menu', methods=('POST', 'GET'))
def menu():
    session['adventure'] = None
    return render_template('index.html', maps=maps)

@app.route('/game', methods=['POST', 'GET'])
def game():
    room_name = session.get('room_name')
    if request.method == "GET":
        if room_name:
            room = planisphere.load_room(room_name)
            return render_template("show_room.html", room=room)
    else:
        action = request.form.get('action')
        if room_name and action:
            room = planisphere.load_room(room_name)
            
            if room.attempts is None:
                next_room = room.go(action)
            else:
                if room.attempts == 0:
                    next_room = room.go('*')
                else: 
                    next_room = room.go(action)
                    room.attempts -= 1
                    
            if not next_room:
                session['room_name'] = planisphere.name_room(room)
            else:
                session['room_name'] = planisphere.name_room(next_room)
        
        return redirect(url_for("game"))    

@app.route('/help', methods=['GET'])
def get_help():
    current_room = session.get('room_name')
    return jsonify(planisphere.load_room_help(current_room))
    
app.secret_key = "A234jadfj sdf/290I)#*/,W?r9"

if __name__ == "__main__":
    app.run()
