try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My First Website with Python',
    'author': 'Zach Berwaldt',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'zberwaldt@tutamail.com',
    'version': '0.1',
    'install_requires': ['pytest', 'flask'],
    'packages': ['gothonweb'],
    'scripts': [],
    'name': 'gothonweb'
}

setup(**config)