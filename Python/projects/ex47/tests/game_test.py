import pytest
from ex47.game import Room


def test_room():
    gold = Room("GoldRoom", 
                """This room has gold in it you can grab, there is a door to the north""")
    assert gold.name == "GoldRoom"
    assert gold.paths == {}

def test_room_paths():
    center = Room("center", "Test room in the center")
    north = Room("north", "Test room in the north")
    south = Room("south", "Test room in the south")
    center.update_paths({"north": north, "south": south})
    assert center.go("north") == north
    assert center.go("south") == south


def test_map():
    start = Room("Start", "You can go west and down a hole.")
    west = Room("Trees", "There are trees here. you can go east")
    down = Room("Dungeon", "It's dark down here you can go up.")

    start.update_paths({"west": west, "down": down})
    west.update_paths({'east': start})
    down.update_paths({'up': start})
    assert start.go('west') == west
    assert start.go('west').go('east') == start
    assert start.go('down').go('up') == start

def setup():
    print("SETUP!")

def teardown():
    print("TEAR DOWN!")

def test_basic():
    print("I RAN!", end='')