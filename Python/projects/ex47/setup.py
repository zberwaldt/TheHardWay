try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Ex47 LPTHW, writing automated tests',
    'author': 'Zach Berwaldt',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'zberwaldt@tutamail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['ex47'],
    'scripts': [],
    'name': 'Ex47'
}

setup(**config)