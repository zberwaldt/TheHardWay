LEXICON = {
    'north': 'direction',
    'south': 'direction',
    'east' : 'direction',
    'west' : 'direction',
    'down' : 'direction',
    'up' : 'direction',
    'left' : 'direction',
    'right' : 'direction',
    'back' : 'direction',
    'go' : 'verb',
    'open': 'verb',
    'stop' : 'verb',
    'kill' : 'verb',
    'eat' : 'verb',
    'the' : 'stop',
    'in' : 'stop',
    'of' : 'stop',
    'from' : 'stop',
    'at' : 'stop',
    'it' : 'stop',
    'door' : 'noun',
    'bear' : 'noun',
    'princess' : 'noun',
    'cabinet' : 'noun'
}

def scan(string):
    
    words = string.split()
    print(words)
    results = []

    for word in words:
        word_type = LEXICON.get(word.lower())

        if word_type == None:
            number = convert_number(word)

            if number != None:
                results.append(('number', number))
            else: 
                results.append(('error', word))
        else:
            results.append((word_type, word))
    
    return results
   

def convert_number(s):
    try:
        return int(s)
    except:
        return None
