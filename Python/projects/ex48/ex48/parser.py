class ParserError(Exception):
    pass

class Sentence(object):

    def __init__(self, subject, verb, obj):
        self.subject = subject[1]
        self.verb = verb[1]
        self.object = obj[1]

    def compile_sentence(self):
        return ' '.join([self.subject, self.verb, self.object])

def peek(word_list):
    # peek takes a list of tuples
    if type(word_list) is not list:
        # if it is not a list raise an exception
        raise ParserError("Expected a list")
    else:
        # word_list seems redundant. if no parameter python will throw error
        if word_list:
            # take first tuple and assign it to word
            word = word_list[0]
            # then return the "type" of word
            return word[0]
        else:
            return None

def match(word_list, expecting):
    # match takes a list of words and the type of word you are expecting
    # pointless if statment?
    if word_list:
        # pop off the first tuple; assign it to word
        word = word_list.pop(0)
        
        # if the word type matches the expected type
        if word[0] == expecting:
            # return it (should be a tuple)
            return word
        else:
            # otherwise return None
            return None
    else:
        # if no word_list return none
        return None

def skip(word_list, word_type):
    # skip takes a word_list, and a word_type to skip (example: stop words)
    while peek(word_list) == word_type:
        # while the peeked word is the word_type. 
        match(word_list, word_type)

def parse_verb(word_list):
    skip(word_list, 'stop')

    if peek(word_list) == 'verb':
        return match(word_list, 'verb')
    else:
        raise ParserError("Expected a verb next.")

def parse_object(word_list):
    # skip the stop words in the word_list
    skip(word_list, 'stop')
    # get the next word
    next_word = peek(word_list)

    # if the next word is a noun or a direction
    if next_word == 'noun':
        return match(word_list, 'noun')
    elif next_word == 'direction':
        return match(word_list, 'direction')
    else:
        # otherwise raise a ParserError exception
        raise ParserError("Expected a noun or direction next.")

def parse_subject(word_list):
    skip(word_list, 'stop')
    next_word = peek(word_list)

    if next_word == 'noun':
        return match(word_list, 'noun')
    elif next_word == 'verb':
        return ('noun', 'player')
    else:
        raise ParserError("Expected a verb next.")

""" 
Pardon the very long note to self: 
Originally I was confused why the code was writen this way. 
However, I forget every sentence has the basic structure
"""

def parse_sentence(word_list):
    subj = parse_subject(word_list)
    verb = parse_verb(word_list)
    obj = parse_object(word_list)

    return Sentence(subj, verb, obj)
