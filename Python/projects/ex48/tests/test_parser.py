import pytest
from ex48 import parser

def test_peek():
    assert parser.peek([('noun', 'player'),
                        ('verb', 'kill'),
                        ('noun', 'bear')]) == 'noun'

def test_peek_exception():
    with pytest.raises(parser.ParserError):
        parser.peek("jkej")    

def test_match():
    assert parser.match([('noun', 'player'),
                        ('verb', 'kill'),
                        ('noun', 'bear')], "noun") == ('noun', 'player')

def test_skip():
    # not sure how to test a function that returns nothing
    pass    

def test_parse_verbs():
    assert parser.parse_verb([('stop', 'the'), ('verb', 'kill')]) == ('verb', 'kill')

def test_parse_verbs_exception():
    with pytest.raises(parser.ParserError):
        parser.parse_verb([('noun', 'doug')])

def test_parse_sentence():
    mock_sentence = parser.parse_sentence([('stop', 'the'), 
                                           ('noun', 'princess'),
                                           ('verb', 'eat'), 
                                           ('stop', 'the'),
                                           ('noun', 'player')]) 
    print(type(mock_sentence))
    assert type(mock_sentence) == parser.Sentence

def test_sentence():
    new_sentence = parser.Sentence(('noun', 'player'),('verb', 'kill'),('noun', 'bear'))
    assert new_sentence.compile_sentence() == "player kill bear"

def setup():
    print("SETUP!")

def teardown():
    print("TEAR DOWN!")

def test_basic():
    print("I RAN!", end='')