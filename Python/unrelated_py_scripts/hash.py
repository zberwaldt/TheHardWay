import hashlib

# mystring = input('Enter String to hash: ')

string1 = "amazing"
string2 = "amazing"
string3 = "amazing"

hash1 = hashlib.md5(string1.encode())
hash2 = hashlib.md5(string2.encode())
hash3 = hashlib.md5(string3.encode())

# Assumes the default UTF-8
# hash_object = hashlib.md5(mystring.encode())

print(hash1.hexdigest())
print(hash2.hexdigest())
print(hash3.hexdigest())