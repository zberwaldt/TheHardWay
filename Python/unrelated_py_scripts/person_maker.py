import json, os

data_path = os.path.abspath('.\\Python\\data.json')

data = open(data_path)

data_contents = data.read()

parsed_data = json.loads(data_contents)

def create_new_person():

    print("What is the persons name?")
    name = input('> ')
    print("And how old are they?")
    age = input('> ')
    done = False
    jobs = []

    while not done:
        print("What is their job(s)?")
        job = input('> ')
        jobs.append(job)
        print("Do they have another job [y/n]?")
        more = input('> ')
        if more != 'y':
            done = True
    
    person = {
        "name" : name,
        "age" : int(age),
        "jobs": jobs
    }
    
    print("Here is the person you created: ", person)
    
    print("Is everything correct? [y/n]")
    
    correct = input('> ')
    
    if correct != 'y':
        edit_person_properties(person)
    else:
        return person


def edit_person_properties(person):
    
    editing = True
    
    while editing:
        
        print("What would you like to edit? [name / age / jobs]")
        
        field_to_edit = input('> ')
                
        if field_to_edit == "name":
            
            print("What is the new name?")
            
            name = input('> ')
            
            person['name'] = name
            
            print("Here is the updated person: ", person)
            
            print("Do you still want to edit? [y /n]")


        elif field_to_edit == "age":
        
            print("What is the new age?")
            
            age = input('> ')
            
            person['age'] = age
            
            print("Here is the updated person: ", person)
            
            print("Do you still want to edit? [y /n]")
        
        elif field_to_edit == "jobs":
            print("here are the jobs:")
            
            for job in person['jobs']:
            
                print("\t*", job)
            
            print("What do you want to change? [add / edit]")

            action = input('> ')
            
            if action == "add":
                
                print("What job would you like to add?")
                
                new_job = input('> ')
                
                person['jobs'].append(new_job)

            elif action == "remove":
                
                print("What job do you want to remove?")
                
                remove_job = input('> ')
                
                person['jobs'].remove(remove_job)

            else:
                print("Please enter a valid option.")

        elif field_to_edit is None:

            return person

        else:
            print("That is not an option")


new_person = create_new_person()

parsed_data['people'].append(new_person)

new_data = open(".\\Python\\data.json", 'w')

new_data.write(json.dumps(parsed_data, indent=4, sort_keys=True))

data.close()
new_data.close()