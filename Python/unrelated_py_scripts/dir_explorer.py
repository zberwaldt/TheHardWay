import os
from pprint import pprint
import re

file_path = os.path.dirname(os.path.realpath(__file__))

downloads_folder = os.path.relpath('C:\\users\\rag\\Downloads', file_path)

file_regex = re.compile(r'''\w+?.unitypackage''')

downloads_files = os.listdir(downloads_folder)

for file in downloads_files:
    mo = file_regex.search(file)
    if mo is not None:
        print(mo.group())

#pprint(downloads_files)