from math import floor
from functools import reduce

def binary_search(array, n, target):
    """
    One implementation of a binary search algorithm, in python.
    """
    array.sort()
    # print(array)
    leftmost = 0
    rightmost = n - 1
    while leftmost <= rightmost:
        m = floor((leftmost + rightmost) / 2 )
        if array[m] < target:
            leftmost = m + 1
        elif array[m] > target:
            rightmost = m - 1
        else:
            return m
    return "unsuccessful"


arr1 = [1, 34, 2, 3, 55, 6, 7, 9, 0]
arr2 = [3, 5, 8, 9, 10, 15, 17]
arr3 = [0,1,2,3,4,8]
arr4 = ['ab', 'zbe', 'lit', 'qiz', 'ode', 'pin', 'cinder', 'vindicate', 'monty']

# print(binary_search(arr1, len(arr1), 55))
# print(binary_search(arr2, len(arr2), 8))
# print(binary_search(arr3, len(arr3), 4))
# print(binary_search(arr4, len(arr4), 'pin'))


def add_together(*args):
    """
    Sums two arguments, but if only one is supplied it returns another function that expects one argument and returns the sum
    """
    arguments = list(args)
    if not all(type(i) is int for i in arguments):
        None
    else:
        if len(arguments) > 1:
            return reduce(lambda x, y: x + y, arguments)
        else:
            return lambda n: n + arguments[0] if type(n) is int else None

    # I tried to do it in one line, but it became really hard to read and hard to debug, so I opted for a slightly more verbose version.
    # return None if not all(type(i) is int for i in arguments) else reduce(lambda x, y: x + y, arguments) if len(arguments) > 1 else n + arguments[0] i


# print(add_together(1)(2)) >> 3
# print(add_together(2)([2])) >> None
# print(add_together(2, "3")) >> None
# print(add_together(1,2,3,4)) >> 10
# print(add_together("http://bit.ly")) >> None

