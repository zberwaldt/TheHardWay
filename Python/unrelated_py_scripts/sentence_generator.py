from random import randint

LEXICON = {
    'north': 'direction',
    'south': 'direction',
    'east' : 'direction',
    'west' : 'direction',
    'down' : 'direction',
    'up' : 'direction',
    'left' : 'direction',
    'right' : 'direction',
    'back' : 'direction',
    'go' : 'verb',
    'open': 'verb',
    'stop' : 'verb',
    'kill' : 'verb',
    'eat' : 'verb',
    'the' : 'stop',
    'in' : 'stop',
    'of' : 'stop',
    'from' : 'stop',
    'at' : 'stop',
    'it' : 'stop',
    'door' : 'noun',
    'bear' : 'noun',
    'princess' : 'noun',
    'cabinet' : 'noun'
}

nouns = []
stops = []
verbs = []

for key, value in LEXICON.items():
    if value == 'stop':
        stops.append(key)
    elif value == 'verb':
        verbs.append(key)
    else:
        nouns.append(key)


def generate_sentence():
    noun = nouns[randint(0, len(nouns) - 1)]
    verb = verbs[randint(0, len(verbs) - 1)]
    stop = stops[randint(0, len(stops) - 1)]

    return verb + ' ' + stop + ' ' + noun


print(generate_sentence())
print(generate_sentence())
print(generate_sentence())