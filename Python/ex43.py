# * Map
#     - next_scene
#     - opening_scene
# * Engine
#     - play
# * Scene
#     - enter
#     * Death
#     * Central Corridor
#     * Laser Weapon Armory
#     * The Bridge
#     * Escape Pod

from sys import exit
from random import randint
from textwrap import dedent
from time import sleep
from math import floor
import os

# TODO: Build Combat system 
# Create a class for Lifeform, which the Player and Gothons belong to.
class Lifeform(object):

    def __init__(self, health):
        self.health = health
        self.strength = randint(1,3)
        self.agility = randint(1,3)
    
    # method for swinging, which has a base damage of 10 times the strength divided by the targets agility
    def swing(self, target_agility):
        damage = floor((10 * self.strength) / target_agility)
        return damage


class Scene(object):
    
    # My train of thinking here is that a Gothon could appear in any scene
    # which is why I instantiate it here. Then pass in the player, which..
    # will be instantiated in the Map, so it'll persist across scenes.
    
    def __init__(self, player, has_gothon=False):
        
        self.player = player
        
        if(has_gothon):
            self.scene_gothon = Lifeform(50)

    def enter(self):
        
        """
        This function is where the story for a scene is printed, and 
        user input is gathered to drive the story down it's different
        branches.
        """
        
        print("This scene is not yet configured.")
        
        print("Subclass it and implement enter()>")
        
        exit(1)
    
    # I started to implement a combat loop method, 
    # but haven't full worked out the details.
    def do_combat(self, scene_name, player, enemy):
        
        """
        Combat loop. Takes the player, the gothon. and runs them through
        logic and odds.
        """
        in_combat = True
        
        while in_combat:
            
            if player.health <= 0:
            
                return 'death'
            
            elif enemy.health <= 0 or in_combat == False:
            
                return scene_name

        print("Implement combat method in Scene subclass.")
        
        exit(1)

        

class Engine(object):

    """
    This is the game engine, that takes a Map and loops through until 
    the end scene is reached.
    """
    
    def __init__(self, scene_map):
        
        self.scene_map = scene_map

    def play(self):
        
        """
        The play method is where the game is started and loops through 
        all the scenes in the map.
        """

        print(f"Player has {self.scene_map.player.health} health points left")
        
        current_scene = self.scene_map.opening_scene()
        
        last_scene = self.scene_map.next_scene('finished')

        while current_scene != last_scene:
        
            next_scene_name = current_scene.enter()
        
            current_scene = self.scene_map.next_scene(next_scene_name)
    
        current_scene.enter()


class Death(Scene):
    
    quips = [
        "You died. You kinda suck at this.",
        "Your Mom would be proud...if she were smarter.",
        "Such a luser",
        "I have a small puppy that's better at this.",
        "You're worse than your Dad's jokes."
    ]

    def enter(self):
    
        print(Death.quips[randint(0, len(self.quips)-1)])
    
        exit(1)

class CentralCorridor(Scene):

    def enter(self):
       
        # I know that the inital health of a Gothon is 50, so I can 
        # create a if else to account for first entering the scene. 
        # I feel like this is messy, but it's the simplest solution 
        # at this time.
       
        if self.scene_gothon.health == 50:
       
            print(dedent(f'''
                You snap out of a daze, and your just standing there in a
                corridor, self narrating with bad grammar.
                In front of you is a "Gothon",
                (You don't know what the creature is, you just remember the
                word "Gothon" from a book you read once.)
            '''))
       
        else:
       
            print(dedent(f''' 
                You've stepped back to reflect on your life choices. Still the
                Gothon is in front of you. Surprisingly it hasn't pressed the
                attack. By your estimation the beast has{self.scene_gothon.health}
                health points left. You still have some shreds of modesty to hurl.
            '''))
        
        action = input("> ")

        if action == "shoot":
       
            print(dedent('''
                Forgetting that you don't have a gun, 
                you accidentally rip off your belt.
                Your pants drop, exposing youself to the Gothon.
                In you're embaressment, you clamour around trying to recover 
                the few shreds of modesty you have left.
                You hurl your shreds of modesty at the Gothon. 
            '''))

            # I could probably spin this while loop off into it's own function
       
            while self.scene_gothon.health >= 0:
                
                player_damage = self.player.swing(self.scene_gothon.agility)
                
                self.scene_gothon.health -= player_damage
                
                # add check Gothons health amount.
       
                if self.scene_gothon.health > 0:
       
                    print(dedent(f'''
                        The Gothon has survived your attack of: 
                        {player_damage} damage points.
                        It has {self.scene_gothon.health} health points left. 
                        It counter-attacks with flatulence...
                    '''))
                    
                    # I should assign self.scene_gothon to a variable.
                    # Should make readability better.
                    gothon_damage = self.scene_gothon.swing(self.player.agility)
                    
                    self.player.health -= gothon_damage
                    
                    print(dedent(f'''
                        MY GOD IT SMELLS LIKE SOMETHING CRAWLED UP 
                        THAT GOTHON'S ASS AND DIED!!!
                        The Gothon hits you for {gothon_damage} points...
                        You now have {self.player.health} health points left
                    '''))

                    if self.player.health <= 0:
       
                        print("The Gothon has farted you to death.")
       
                        return 'death'
                    
                    print("Do you want to continue fighting?")
                    
                    action = input('> ')
                    
                    if action != "shoot":
       
                        return 'central_corridor'

            print("You've defeated the Gothon the way is clear.")

            return 'laser_weapon_armory'    
                
        elif action == "dodge":

            print(dedent('''
                You dodge, forgetting that you are in a corridor, 
                and don't have room to maneuver. 
                You crash into the wall and break your neck. 
            '''))

            return 'death'

        elif action == "walk past":

            print(dedent('''
                You take a chance and start walking toward the Gothon. 
                You start humming the battle theme from James Bond, 
                trying to increase the drama.
                Well past the Gothon, you continue on your journey.
            '''))
            
            return 'laser_weapon_armory'
        
        else:
        
            print('DOES NOT COMPUTE!')
        
            return 'central_corridor'


class LaserWeaponArmory(Scene):

    def enter(self):
        
        print(dedent('''
            Having conquered your first challenge, 
            you've decided to go to the armoury and retrieve a neutron bomb.
            You've elected to destroy the ship on a whim, 
            fudge these new lifeforms and fudge this ship!
            For reasons that escape memory, there is a keypad on 
            the neutron bomb box. 
            Blast! You can't remember the password. 
            You shout, 
            "The question is: Do you feel the odds are favorable?... Well? 
            do ya? Punk!"
            There was something else you wanted to say, but was afraid of 
            copyright infringement claims.
        '''))

        # Randomly generate a key code for the player to guess
        
        keypad_code = f"{randint(1,9)}{randint(1,9)}{randint(1,9)}"
        
        # Get the guess of the user
        
        guess = input("[keypad]> ")
        
        # initialize guesses variable to track the number of guess
        
        guesses = 1

        # now start a loop, which will terminate or skip if the guess matches 
        # the random code, the "cheat" or when guess excedes 10
        
        while guess != keypad_code and guess != "open door" and guesses < 10:
        
            print(dedent(f'''
                "Beep Boop Boop! Guess Again", the keypad chimes.
                {guesses} is the lucky try.
            '''))
        
            guesses += 1
        
            guess = input("[keypad]> ")
        
        if guess == keypad_code:
        
            print(dedent('''
                Success! Time to blow up this damn ship.
            '''))

            sleep(2)
        
            os.system('cls')
        
            return 'the_bridge'
        
        elif guess == "open door":
        
            print(dedent('''
                Turns out, you're an idiot, and SOMEONE never fully closed the door. 
                Any normal person would resign from this job for negligence.
                But you're not a normal person.
            '''))

            sleep(2)
        
            os.system('cls')

            return 'the_bridge'
        
        else:
        
            print(dedent('''
                You've used up your last attempt. 
                The screen reads: "LOCKED".
                With nothing left to do, and displeased that you were unable to blow up the ship.
                You rush to an airlock and eject yourself into the cold embrace of space.
            '''))

            return 'death'

    
class TheBridge(Scene):
    
    def enter(self):

        print(dedent('''
            You've entered the "Bridge", which is actually the lavatory.
            If only Upper Management knew how much time you've spent here, 
            avoiding work, you would have been jobless years ago.
            You start walking towards "The Captain's Chair" 
            A Gothon exits, and you pause to decide how to proceed.
        '''))

        action = input('> ')

        if action == "shoot":

            print(dedent('''
            You try to shoot the Gothon, but because you are clumsy you end up 
            shooting yourself in the eye. Leading to blindness and unemployment.
            '''))

            return 'death'

        elif action == 'walk past':

            print(dedent('''
                Hey, it worked for the last one!
                You attempt to walk past but as you get closer you begin to smell "it"
                "It" can only be described as the most fowl stench that has ever graced your nostrils.
                It was truly heinous.
                You turn tail and run, tears in your eyes, because your beloved "Captain's Chair" on the "Bridge"
                was violated, never to be the same again.
            '''))

            return 'death'

        elif action == 'douse':
            print(dedent('''
                You shoot a glance around "The Bridge", on the counter 
                by the sink is a can of air refreshener!
                "YOU'LL PAY FOR THIS FOUL CREATURE!" you shout.
                The Gothon, repulsed by the smell of fresh, bolts for the door
                Screaming and Howling all the way back from whence it came.
            '''))

            return 'escape_pod'

        else:
            
            return 'the_bridge'
    
class EscapePod(Scene):

    def enter(self):
        print(dedent('''
        You've arrived to an area you've dubbed the "Escape Pods". Mostly, 
        becuase it is the Escape Pods. However, due to previous workplace 
        behavior you've been banned indefinitely from the area. There are five 
        pods to choose from. You recall overhearing some technicians talking 
        about technical issues about escape pods, but they are definitely more 
        responsible and hard working than you, so it shouldn't be a problem.
        '''))
        
        action = input('> ')

        good_pod = f"{randint(1,5)}"

        if action == good_pod:

            print(dedent('''
                You vault into the escape pod, smashing the eject button 
                As you are being sent careening out into space, 
                you take one last look back. Remembering "The Bridge" and 
                all the procrastination preformed there.
            '''))
            
            return 'finished'

        else:

            print(dedent("""
            You vault into the escape pod and smash the eject button. It triggers an alarm and popobots (police bots) come in 
            and arrest you for trespassing.
            (which you pronounce as "trespessing", becuase you're a douche.).
            """))
            
            return 'death'

class Finished(Scene):

    def enter(self):
        
        print(dedent('''
            A few months has passed, and the oversight committee has 
            found insufficent evidence to prove negilgence and insanity.
            However, to prevent you from being in control of spaceships 
            again, they've revoked your spaceship license indefinitely.
            Jobless and Homeless, you look to the future. 

            You won! Good job.
        '''))
        
        return 'finished'

class Map(object):
    
    def __init__(self, start_scene):
        
        # the __init__ method is where you initialize object properties.
        
        self.start_scene = start_scene
        
        self.player = Lifeform(50)

        # Create a dictionary that will house all the different Scenes.
        # This is how I'll able to access them and determine the next_scene
        # I moved this into the init funciton so I could pass the player to 
        # each scene, I bet this probably could be done more efficiently, 
        # but for now it'll do... 

        self.scenes = {
            'central_corridor': CentralCorridor(self.player, True),

            'laser_weapon_armory' : LaserWeaponArmory(self.player),

            'the_bridge': TheBridge(self.player, True),

            'escape_pod': EscapePod(self.player),

            'death': Death(self.player),

            'finished': Finished(self.player)
        }

    def next_scene(self, scene_name):

        # When this method is called it gets the object from the dictionary and returns it.

        val = self.scenes.get(scene_name)

        return val
    
    def opening_scene(self):

        # call the the next_scene method (self is similar to this in JS). Pass in the start_scene property you created in the __init__ method.

        return self.next_scene(self.start_scene)

# Create an instance of a Map and pass in the starting scene.

a_map = Map('central_corridor')

# Create an instance of Engine and pass in a_map, which is an instance of Map

a_game = Engine(a_map)

# Initialize the game loop, by callling the play method of the a_game Engine

a_game.play()
