# i = 0
# numbers = []

# while i < 6:
#     print(f"At the top i is {i}")
#     numbers.append(i);

#     i += 1
#     print("Numbers now: ", numbers)
#     print(f"At the bottom i is {i}")

# print("The numbers: ")

# for num in numbers: 
#     print(num)

# Study Drill

# Rewrite above as function

def build_array(num):
    i = 0
    numbers = []

    while i < num:
        print(f"At the top i is {i}")
        numbers.append(i)
            
        i += 1
        
        print("Numbers now: ", numbers)
        print(f"At the bottom i is {i}")
    
    print("The numbers: ")

    for entry in numbers:
        print(entry)

build_array(20)
build_array(3)