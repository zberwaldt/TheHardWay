# README

This repository is all about learning ruby.

I've been reading a book called Learn Ruby The Hard Way by Zed Shaw, all of the
files in this project are from the exercises contained in that book.

For anyone starting out or trying to add Ruby to their tool box, I highly recommend it. 

## 3\2\2018

This is a few days old, but I switched to learning Python instead. I did this because I feel that Python is probably the better language to learn given it's uses in Data Science, and it's getting quite popular as a result.

I still plan on learning Ruby, it's just lower on the stack of priorities now.

I think the two are pretty similar (at least in terms of the basics, I'm sure their difference will become more apparent in time)
So I don't think the transistion will be that hard, and It's really not about the languages, anyways. It's more import to learn the concepts. yada yada yada.

